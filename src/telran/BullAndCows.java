package telran;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.min;

public class BullAndCows {

    private int[] number;
    private final List<GameLog> logs = new ArrayList<>();
    private final static String LOG_DIR = "C:\\JAVA_PROJECTS\\117-35-BullsAndCows\\";
    private static final int NUMBER_LENGTH = 4;


    public void game() throws FileNotFoundException {
        number = randomNumber();
        inputMode();
        System.out.printf("%d moves, number: %s", logs.size(), getNumber());
        saveToFile();
    }

    private void inputMode() {
        Scanner scanner = new Scanner(System.in);
        boolean chkNumber = true;
        while (chkNumber) {
            System.out.println("Enter number:");
            String line = scanner.nextLine();
            if (line.equals("debug")) System.out.println("Debug mode, number is: " + getNumber());
            else chkNumber = chkNumber(line);
        }
    }

    private Boolean chkNumber(String Numbers) {
        int length = min(Numbers.length(), NUMBER_LENGTH);
        int Cows = 0;
        int Bulls = 0;
        for (int i = 0; i < NUMBER_LENGTH; i++) {
            for (int j = 0; j < length; j++) {
                int number = Character.getNumericValue(Numbers.charAt(j));
                if (this.number[i] == number) {
                    if (i == j) {
                        Bulls++;
                    } else Cows++;
                }
            }
        }
        logs.add(new GameLog(Cows, Bulls, Numbers));
        getPreviousMove();
        return Bulls != length;
    }

    private void getPreviousMove() {
        for (GameLog log : logs) {
            System.out.printf("previous move: %s (%d cows, %d bulls)\n ",
                    log.getNumber(),
                    log.getCows(),
                    log.getBulls());
        }
    }


    private int[] randomNumber() {
        return new Random().ints(1, 10)
                .distinct()
                .limit(NUMBER_LENGTH).toArray();
    }

    private void saveToFile() throws FileNotFoundException {
        String number = getNumber();

        PrintStream writer = new PrintStream(getFileName());
        writer.println("Set number: " + number);
        for (GameLog log : logs) {
            writer.printf("Number: %s Cows: %d Bulls: %d\n",
                    log.getNumber(), log.getCows(), log.getBulls());
        }
        writer.println("moves: " + logs.size());
        writer.close();
    }

    private String getNumber() {
        StringBuilder res = new StringBuilder();
        for (int value : number) {
            res.append(value);
        }
        return res.toString();
    }

    private String getFileName() {
        LocalDateTime date = LocalDateTime.now();
        return String.format(
                "%s%d-%d-%d_%d_%d_%d.txt",
                LOG_DIR,
                date.getYear(),
                date.getMonthValue(),
                date.getDayOfMonth(),
                date.getHour(),
                date.getMinute(),
                logs.size());
    }
}