package telran;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class GameLog {
    private static final Object LOG_DIR = "C:\\JAVA_PROJECTS\\117-35-BullsAndCows\\";;
    private int cows;
    private int bulls;
    private String number;


    public GameLog(int cows, int bulls, String number) {
        this.cows = cows;
        this.bulls = bulls;
        this.number = number;
    }

    public int getCows() {
        return cows;
    }

    public int getBulls() {
        return bulls;
    }

    public String getNumber() {
        return number;
    }

}