package telran.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpBullsCowsServerAppl extends BullsCowsGameImpl {

    private static final int PORT = 9001;


    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(PORT);
        System.out.println("Server is listening on port " + PORT);
        while (true) {
            Socket socket = serverSocket.accept();
            runClient(socket);
        }
    }


    private static void runClient(Socket socket) {

        BullsCowsGameImpl bullsCows = new BullsCowsGameImpl();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintStream writer = new PrintStream(socket.getOutputStream())) {
            while (true) {
                String request = reader.readLine();
                if (request == null) {
                    break;
                }
                String response = getResponse(request, bullsCows);
                writer.println(response);
            }

        } catch (IOException e) {
            System.out.println("illegal way of closing connection");
            return;
        }

        System.out.println("client closed connection");
    }


    private static String getResponse(String request, BullsCowsGameImpl bullsCows) {

        String[] headersPayload = request.split("#");
        String headers = headersPayload[0];
        String payload = headersPayload[1];
        if (headersPayload.length != 2) return "Unknown Request";

        return switch (headers) {
            case "startGame" -> bullsCows.startGame();
            case "move" -> bullsCows.move(payload);
            case "isFinished" -> String.valueOf(bullsCows.isFinished());
            case "prompt" -> bullsCows.prompt();
            default -> "Unknown Request";
        };
    }
}
