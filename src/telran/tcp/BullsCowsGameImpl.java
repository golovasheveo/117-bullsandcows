package telran.tcp;

import telran.GameLog;

import java.util.Random;

import static java.lang.Math.min;

public class BullsCowsGameImpl implements GuessGame {

    private static final int NUMBER_LENGTH = 4;
    private final int[] number;
    public boolean isFinished = false;

    BullsCowsGameImpl() {
        number = randomNumber();
    }

    private String chkNumber(String Numbers) {
        int length = min(Numbers.length(), NUMBER_LENGTH);
        int Cows = 0;
        int Bulls = 0;
        for (int i = 0; i < NUMBER_LENGTH; i++) {
            for (int j = 0; j < length; j++) {
                int number = Character.getNumericValue(Numbers.charAt(j));
                if (this.number[i] == number) {
                    if (i == j) {
                        Bulls++;
                    } else Cows++;
                }
            }
        }
        if (Bulls == length) isFinished = true;
        return new GameLog(Cows, Bulls, Numbers).toString();
    }


    private int[] randomNumber() {
        return new Random().ints(1, 10)
                .distinct()
                .limit(NUMBER_LENGTH).toArray();
    }

    String getNumber() {
        StringBuilder res = new StringBuilder();
        for (int value : number) {
            res.append(value);
        }
        return res.toString();
    }


    @Override
    public String startGame() {
        return "startGame";
    }

    @Override
    public String prompt() {
        return getNumber();
    }

    @Override
    public String move(String userInput) {
        return chkNumber(userInput);
    }

    @Override
    public boolean isFinished() {
        return isFinished;
    }
}
