package telran.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class GameTcpProxy implements GuessGame {

    private final PrintStream writer;
    private final BufferedReader reader;

    public GameTcpProxy(int PORT, String HOST) throws IOException {
        Socket socket = new Socket(HOST, PORT);
        writer = new PrintStream(socket.getOutputStream());
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    @Override
    public String startGame() throws IOException {
        writer.println("startGame#.");
        return reader.readLine();
    }


    @Override
    public String prompt() throws IOException {
        writer.println("prompt#.");
        return reader.readLine();
    }


    @Override
    public String move(String userInput) throws IOException {
        writer.println("move#" + userInput);
        return reader.readLine();
    }


    @Override
    public boolean isFinished() throws IOException {
        String response = "";
        writer.println("isFinished#.");
        response = reader.readLine();
        return response.equals("true");
    }
}
