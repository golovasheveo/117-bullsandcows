package telran.tcp;

import java.io.IOException;

public interface GuessGame {

    String startGame() throws IOException; //promt for user input

    String prompt() throws IOException; //result for displaying

    String move(String userInput) throws IOException; // result for displaying

    boolean isFinished() throws IOException;
}
