package telran.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TcpGameConsoleClientAppl extends GameTcpProxy {
    private static final List<String> listLog = new ArrayList<>();
    private static final int TCP_PORT = 9001;
    private static final String HOST_NAME = "localhost";
    private static BufferedReader reader;

    public TcpGameConsoleClientAppl(int PORT, String HOST) throws IOException {
        super(PORT, HOST);
    }


    public static void main(String[] args) throws IOException {
        reader = new BufferedReader(new InputStreamReader(System.in));
        runConsole();
    }


    private static void runConsole() throws IOException {
        GameTcpProxy guessGame = new GameTcpProxy(TCP_PORT, HOST_NAME);
        if (guessGame.startGame().equals("startGame")) {
            while (!guessGame.isFinished()) {
                System.out.println("Enter number:");
                String scanner = reader.readLine();
                if (scanner.equals("debug")) System.out.println("Debug mode, number is: " + guessGame.prompt());
                String response = guessGame.move(scanner);
                listLog.add(response);

                printLog();
            }

        } else System.out.println("Server error connect");
    }

    private static void printLog() {
        TcpGameConsoleClientAppl.listLog.forEach(System.out::println);
    }
}
